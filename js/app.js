const quiz = document.getElementById('form__quiz');
const next = document.getElementById('proximo');
const previous = document.getElementById('anterior');

// SECTIONS
const quizHome = document.querySelector('.form')
const newsletter = document.querySelector('.newsletter');
const product01 = document.querySelector('.product01');
const product02 = document.querySelector('.product02');

// BUTTONS
const btnNextNewsletter = document.querySelector('.newsletter__btn #proximo');
const btnPrevNewsletter = document.querySelector('.newsletter__btn #anterior');

var questions = [{
        question: "O que você procura em um tratamento capilar?",
        answers: ["RECONSTRUÇÃO", "NUTRIÇÃO", "HIDRATAÇÃO", "PROTEÇÃO DA COR"],
        number: 1
    },
    {
        question: "Qual é a espessura do seu fio?",
        answers: ["FINO", "GROSSO"],
        number: 2
    },
    {
        question: "Qual é o tipo do seu cabelo?",
        answers: ["NATURAL", "MECHAS OU LUZES", "COM COLORAÇÃO"],
        number: 3
    },
    {
        question: "Qual é o comprimento do seu cabelo?",
        answers: ["CURTO", "MÉDIO", "LONGO"],
        number: 4
    },
    {
        question: "Qual é o tipo do seu cabelo?",
        answers: ["LISO", "ONDULADO", "CACHEADO", "CRESPO"],
        number: 5
    }
];

function createHtml(data, index) {
    let i = index;
    var answers = data[i].answers;

    return (` <span id="pageNumber">${data[i].number}</span>
    <p for="pagination__text">${data[i].question}</p>
    <div class="box">
    ${answers.map(function (answer, index) {
        return `
            <div class="input__box">
                <input type="radio" id="resposta${index}" name="resposta${i}" value="${answer}" required="required">
                <label for="resposta${index}">${answer}</label>
            </div>
      `
    }).join('')}
   `);
}

function renderHtml(data, index) {
    const render = createHtml(data, index);
    quiz.innerHTML = render;
}

renderHtml(questions, 0)

// SELECT VALUE INPUT
let valueInput = false;
let step1 = '';
let step2 = '';
let step3 = '';
let step4 = '';
let step5 = '';
let inputEmail;
let inputTerms;

// EVENT CLICK FOR BUTTON NEXT FORM
next.addEventListener('click', function (event) {
    event.preventDefault();
    let number = parseInt(document.querySelector('#pageNumber').innerHTML);
    const limit = questions.length;
    let inputRequired = document.querySelectorAll(`input[name="resposta${number - 1}"]`);

    for (let i = 0; i < inputRequired.length; i++) {
        if (inputRequired[i].checked === true) {
            number++;
        }
    }

    const btnForm = document.querySelector('.form__btn #anterior');

    if (number === 2) {
        step1 = quiz.resposta0.value;
        btnForm.classList.remove('invisible');
    } else if (number === 3) {
        getInputValue();
        step2 = quiz.resposta1.value;
    } else if (number === 4) {
        step3 = quiz.resposta2.value;
    } else if (number === 5) {
        step4 = quiz.resposta3.value;
    } else if (number === 6) {
        step5 = quiz.resposta4.value;
    }

    if (number > limit) {
        number = limit;
        quizHome.classList.add('invisible');
        newsletter.classList.remove('invisible');
    }

    const index = number - 1;

    renderHtml(questions, index);

    var numbersList = document.querySelectorAll("li");
    numbersList.forEach(function (li) {
        li.classList.remove('active');
    })

    numbersList[index].classList.toggle('active');
})

function getInputValue() {
    let step2 = quiz.resposta1.value;
    if (step2 == 'FINO') {
        valueInput = true;
    } else {
        valueInput = false;
    }
}

// EVENT CLICK FOR BUTTON PREVIOUS FORM
previous.addEventListener('click', function () {
    let number = parseInt(document.querySelector('#pageNumber').innerHTML);
    number--;
    if (number < 1) number = 1
    const index = number - 1
    renderHtml(questions, index);

    const btnForm = document.querySelector('.form__btn #anterior');

    if (number === 1) {
        btnForm.classList.add('invisible');
    }

    var numbersList = document.querySelectorAll("li");
    numbersList.forEach(function (li) {
        li.classList.remove('active')
    })

    numbersList[index].classList.toggle('active');
});

btnNextNewsletter.addEventListener('click', function () {
    if (valueInput == true) {
        newsletter.classList.add('invisible');
        product01.classList.remove('invisible');
        inputEmail = document.querySelector('.newsletter__email__input').value;
        inputTerms = document.querySelector('.newsletter__term__input').checked;
    } else {
        newsletter.classList.add('invisible');
        product02.classList.remove('invisible');
        inputEmail = document.querySelector('.newsletter__email__input').value;
        inputTerms = document.querySelector('.newsletter__term__input').checked;
    }
});

btnPrevNewsletter.addEventListener('click', function () {
    quizHome.classList.remove('invisible');
    newsletter.classList.add('invisible');
});

let btnOkFino = document.querySelector('.btn__fino');
let btnOkGrosso = document.querySelector('.btn__grosso');

btnOkFino.addEventListener('click', function () {
    const url = `https://landfy.smartcampaign.com.br/landfy/api/41f7bb34-d8d8-11ea-903f-0e128ad531c1?fields[Email]=${inputEmail}&fields[Tratamento]=${step1}&fields[Espessura]=${step2}&fields[Cabelo]=${step3}&fields[Comprimento]=${step4}&fields[Tipo]=${step5}&fields[Optin]=${inputTerms}&unique=Email`;

    const _options = {
        method: 'GET',
        mod: 'cors'
    }

    fetch(url, _options)
        .then((response) => response.json())
        .then((result) => {
            console.log(result);
        });
});

btnOkGrosso.addEventListener('click', function () {
    const url = `https://landfy.smartcampaign.com.br/landfy/api/41f7bb34-d8d8-11ea-903f-0e128ad531c1?fields[Email]=${inputEmail}&fields[Tratamento]=${step1}&fields[Espessura]=${step2}&fields[Cabelo]=${step3}&fields[Comprimento]=${step4}&fields[Tipo]=${step5}&fields[Optin]=${inputTerms}&unique=Email`;

    const _options = {
        method: 'GET',
        mod: 'cors'
    }

    fetch(url, _options)
        .then((response) => response.json())
        .then((result) => {
            console.log(result);
        });
});